import Dialog_class from './../../libs/popup.js';

class Utente_class {

    constructor() {
        this.POP = new Dialog_class();
    }


    registrazione() {
        var settings = {
            title: 'Test registrazione',
            params: [
                {name: "inserimento nome", title: "Inserisci nome:", value: ""},
            ],
        };
        this.POP.show(settings);
    }

}

export default Utente_class;
