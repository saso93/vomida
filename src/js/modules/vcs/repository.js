
import Base_layers_class from "../../core/base-layers";
import Helper_class from "../../libs/helpers";
import Dialog_class from "../../libs/popup";
import config from './../../config.js';

class VCS_class {

    constructor() {

        this.POP = new Dialog_class();
        this.set_events();

    }

    set_events() {
        var _this = this;

        document.addEventListener('keydown', function (event) {
            var code = event.keyCode;
            if (event.target.type == 'text' || event.target.tagName == 'INPUT' || event.target.type == 'textarea')
                return;

            if (code == 83) {
                //save
                _this.repository();
                event.preventDefault();
            }
        }, false);
    }



    repository() {
        var _this = this;
        this.POP.hide();
        var settings = {
            title: 'Save as',
            params: [
                {name: "asdasd", title: "File name:", value:"" },
                {name: "name", title: "File name:", value:"" },
            ],

            on_finish: function (params) {
                alert(params.asdasd);
            },
        };

        this.POP.show(settings);

        document.getElementById("pop_data_name").select();
    }

    save_action(user_response) {
        var fname = user_response.name;
        var only_one_layer = null;
        if (user_response.layers == 'All')
            only_one_layer = false;
        else
            only_one_layer = true;
    }





}

export default VCS_class;
